# Contributing!

A guide to helping out with this project!


It's rather simple honestly, I write code on the 'develop' branch. Then when it gets stable enough or I think its ready I merge into 'master'. Generally master then gets merged into 'release' which automatically uploads it into pip.

If you plan to add code, please add it to the 'develop' branch, I'm way more likely to merge it into that branch.

Also note, Only I (sj1k) can merge into 'release'. Which makes sure no nefarious code can get into the live version of the code.
